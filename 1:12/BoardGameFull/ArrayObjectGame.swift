//
//  ArrayObjectGame.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class ArrayObjectGame {
    var ArrayObjectGame  = [ObjectGame]()
    
    func getCell(cell: Cell) -> ObjectGame? {
        for objectGame in ArrayObjectGame {
            if objectGame.cell == cell {
                return objectGame
            }
        }
        return nil
    }
    
    // Change alpha
    func changeAlpha(cell: Cell,alpha: CGFloat) {
        for objectGame in ArrayObjectGame {
            if objectGame.cell == cell {
                objectGame.cell.view.alpha = alpha
            }
        }
    }
    
}
