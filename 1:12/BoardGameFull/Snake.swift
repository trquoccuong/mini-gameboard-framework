//
//  Snake.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class Snake: ObjectGame {
    var length = 5
    override func draw(parrentView: UIView) {
        for i in 0..<length  {
            var snakeBody = Cell(row: board.arrayCell[i].row, col: board.arrayCell[i].col, size: board.sizeCell)
            if ( i != length - 1) {
                snakeBody.view.backgroundColor = UIColor.redColor()
                snakeBody.memo = "body"
            } else {
                snakeBody.view.backgroundColor = UIColor.yellowColor()
                snakeBody.memo = "head"
            }
            snakeBody.setCenter(board)
            arrayCell.append(snakeBody)
            snakeBody.draw(parrentView)
        }
    }
    override func move(){
        var possibleMoves = [Cell]()
        var snakeHead = arrayCell[length - 1]
//        if snakeHead.row > 0 {
//            let leftCell = Cell(row: snakeHead.row - 1, col: snakeHead.col, size: board.sizeCell)
//            if checkCanMove(leftCell) {
//                possibleMoves.append(leftCell)
//            }
//        }
//        
//        if snakeHead.col > 0 {
//            let topCell = Cell(row: snakeHead.row, col: snakeHead.col - 1, size: board.sizeCell)
//            if checkCanMove(topCell) {
//                possibleMoves.append(topCell)
//            }
//            
//        }
//        
//        if snakeHead.row < board.rows - 1 {
//            let rightCell = Cell(row: snakeHead.row + 1, col: snakeHead.col, size: board.sizeCell)
//            if checkCanMove(rightCell) {
//                possibleMoves.append(rightCell)
//            }
//        }
//        
//        if snakeHead.col < board.cols - 1 {
//            let bottomCell = Cell(row: snakeHead.row, col: snakeHead.col + 1, size: board.sizeCell)
//            if checkCanMove(bottomCell) {
//                possibleMoves.append(bottomCell)
//            }
//        }
        

        
        for i in 1...4 {
            var row = snakeHead.row
            var col = snakeHead.col
            
            switch i {
            case 1: row--
            case 2: col++
            case 3: col--
            case 4: row++
            default:
                return
            }
            
            if !((row > board.rows || row < 0) || (col > board.cols || col < 0)) {
                var cell = Cell(row: row, col: col, size: board.sizeCell)
                if checkCanMove(cell) {
                    possibleMoves.append(cell)
                }
            }
            
        }
  //      println("\(countElements(possibleMoves))")

        switch (countElements(possibleMoves)) {
        case 0:
            return //Tit.
        case 1:
            moveAlongTo(possibleMoves[0])
        default:
            let index = arc4random_uniform(UInt32(possibleMoves.count))
            moveAlongTo(possibleMoves[Int(index)])
        }

    }
    
    
    
    func checkCanMove(cell: Cell) -> Bool {
        
        if board[cell.row,cell.col]!.memo == "brick" {
        //    println("\(cell.row),\(cell.col), \(cell.row * board.cols + cell.col),\(board[cell.row,cell.col].row),\(board[cell.row,cell.col].col), ")
            return false
        }
        
        // check body snake
        for cellSnake in arrayCell {
            if cellSnake == cell {
                return false
            }
        }
        return true
    }
}
