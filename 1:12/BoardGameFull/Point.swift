//
//  Point.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 12/31/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class Point {
    var row: Int
    var col : Int
    
    init(_ row: Int ,_ col : Int) {
        self.row = row
        self.col = col
    }
    
}
