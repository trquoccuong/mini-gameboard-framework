//
//  SnakeBoard.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 12/31/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class SnakeBoard: Board {
    override func drawGameBoard(view: UIView) {
        for cell in arrayCell {
            cell.view.frame = CGRectMake(0, 0, sizeCell - marginCell , sizeCell - marginCell)
            var number = arc4random_uniform(16)
            if number != 8 {
                cell.view = UIImageView(image: UIImage(named: "grass"))
                cell.memo = "grass"
            } else {
                cell.view = UIImageView(image: UIImage(named: "brick"))
                cell.memo = "brick"
            }
            cell.setCenter(self)
            cell.draw(view)
        }
    }
}
