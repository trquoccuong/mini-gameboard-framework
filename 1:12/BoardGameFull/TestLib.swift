//
//  TestLib.swift
//  BoardGameFull
//®www.win2.cn/g9

//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import UIKit
var blockT = [Point(1,0), Point(0,1),Point(1,1), Point(1,2), Point(2,1),Point(3,0),Point(3,2)]


class TestLib: UIViewController {
    var gameBoard : Board!
    var objGame: TestObject!
    var objGame2: TestObject!
    var myObj: TestObject!
    var timer : NSTimer!
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor =  UIColor.blackColor()
        
        gameBoard = Board(cols: 8, sizeCell: 30)
        gameBoard.createGame(view.frame)
        gameBoard.drawGameBoard(view)
        
        
        objGame = TestObject(board: gameBoard)
        objGame.creatArrayCell(blockT, startPoint: Point(1,1))
        objGame.setBegin(Point(-1,1))
        objGame.draw(view)
        
        objGame2 = TestObject(board: gameBoard)
        objGame2.creatArrayCell(blockT, startPoint: Point(1,1))
        objGame2.setBegin(Point(-6,7))
        objGame2.draw(view)
        
    
        
        myObj = TestObject(board: gameBoard)
        myObj.creatArrayCell(blockT, startPoint: Point(1,1))
        myObj.setBegin(Point(gameBoard.rows - 2,4))
        for cell in myObj.arrayCell {
            cell.show()
        }
        
        myObj.draw(view)
            
        
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.05, target: self, selector: "update", userInfo: nil, repeats: true)
        
        

        // Do any additional setup after loading the view.
    }

    func update() {
        objGame.move()
        objGame2.move()
    }

}
