//
//  Board.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 12/31/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class Board {
    var cols: Int
    var rows: Int = 0
    var arrayCell = [Cell]() // gameworld  is array cell
    var margin: CGFloat!
    var sizeCell: CGFloat
    var paddingCell: CGFloat = 0
    var marginCell: CGFloat = 0
    var sizeBoard: CGSize!
    
    init(cols: Int, sizeCell: CGFloat){
        self.cols = cols
        self.sizeCell = sizeCell
    }
    
    init(cols: Int, sizeCell: CGFloat, marginCell: CGFloat){
        self.cols = cols
        self.sizeCell = sizeCell
        self.marginCell = marginCell
    }
    
    init(cols: Int, sizeCell: CGFloat, marginCell: CGFloat, paddingCell : CGFloat){
        self.cols = cols
        self.sizeCell = sizeCell
        self.marginCell = marginCell
        self.paddingCell = paddingCell
    }
    
    init(cols: Int,rows : Int,sizeBoard: CGSize) {
        self.cols = cols
        self.rows = rows
        let cellWidth = Int(sizeBoard.width) / cols
        let cellHeight =  Int(sizeBoard.height) / rows
        self.sizeCell = CGFloat(cellWidth > cellHeight ? cellHeight : cellWidth)
    }
    
    
    func createGame(sizeView: CGRect) {
        margin = (sizeView.width - CGFloat(cols) * (sizeCell + marginCell)) * 0.5
        
        if rows == 0 {
            rows =  Int((sizeView.height - 64 - 2 * margin) / (sizeCell +  marginCell))
        }
        
        println("\(rows)")
        for row in 0...rows {
            for col in 0...cols {
                var cell = Cell(row: row, col: col, size: sizeCell)
                arrayCell.append(cell)
            }
        }

        
    }
    
    func drawGameBoard(view: UIView) {
        //overide this for change layout board
        for cell in arrayCell {
            cell.view.frame = CGRectMake(0, 0, sizeCell - paddingCell , sizeCell - paddingCell)
            cell.view.backgroundColor = UIColor.grayColor()
            cell.view.center = CGPointMake(margin + (0.5 + CGFloat(cell.col) * (sizeCell + marginCell) ), 64 + margin + (0.5 + CGFloat(cell.row) * (sizeCell + marginCell)))
            cell.draw(view)
        }
    }
    subscript(row: Int, col: Int) -> Cell?{
        get {
            assert(indexIsValid(row, col: col), "Index out of range")
            for cell in arrayCell {
                if cell.col == col && cell.row == row {
                    return cell
                }
            }
            return nil
        }
//            return self.arrayCell[row * self.cols + col]
    }
    
    func indexIsValid(row: Int, col: Int) -> Bool {
        return ((row <= self.rows  && row >= 0) || (col >= self.cols && col >= 0))
        
    }
    func cellIndexIsValid(cell: Cell) -> Bool {
        return ((cell.row <= self.rows  && cell.row >= 0) || (cell.col >= self.cols && cell.col >= 0))
        
    }
    
    // Check cell have point
    
    func checkPointInCell(point: CGPoint) -> Cell? {
        for cell in arrayCell {
            if cell.view.frame.contains(point) {
                return cell
            }
        }
        return nil
    }
    
    
    
    
}
