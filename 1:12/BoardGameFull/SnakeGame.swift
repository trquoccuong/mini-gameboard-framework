//
//  SnakeGame.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class SnakeGame: UIViewController {
    var boardGame: SnakeBoard!
    var snake: Snake!
    
    var timer: NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackColor()
        boardGame = SnakeBoard(cols: 8, sizeCell: 34)
        boardGame.createGame(view.bounds)
        boardGame.drawGameBoard(view)
        
        snake = Snake(board: boardGame)
        snake.draw(view)
        //
        timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: "update", userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
        
        var tap = UITapGestureRecognizer(target: self, action: "andBrick:")
        self.view.addGestureRecognizer(tap)
        
        let tapOnBrick = UITapGestureRecognizer(target: self, action: "zoom:")
        tapOnBrick.numberOfTapsRequired = 2
        view.addGestureRecognizer(tapOnBrick)
        
        let longPressOnAnt = UILongPressGestureRecognizer(target: self, action: "resetAnt:")
        view.addGestureRecognizer(longPressOnAnt)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: "onSwipe:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Down
        view.addGestureRecognizer(swipeRight)


        
    }
    func update() {
        snake.move()
    }
    
    override func viewWillDisappear(animated: Bool) {
        timer.invalidate()
        timer = nil
    }
    
    func andBrick( tap: UITapGestureRecognizer) {
        let point = tap.locationInView(view)
        if let cellz = boardGame.checkPointInCell(point) {
            if cellz.memo != "brick" {
                cellz.view = UIImageView(image: UIImage(named: "brick"))
                cellz.memo = "brick"
                cellz.setCenter(boardGame)
                cellz.draw(view)
            } else {
                cellz.view = UIImageView(image: UIImage(named: "grass"))
                cellz.memo = "grass"
                cellz.setCenter(boardGame)
                cellz.draw(view)
            }
        }
    }
    func zoom(tap: UITapGestureRecognizer) {
        let point = tap.locationInView(view)
        if let cellz = boardGame.checkPointInCell(point) {
            if cellz.memo == "brick" {
                cellz.view.transform = CGAffineTransformConcat(cellz.view.transform, CGAffineTransformMakeScale(2, 2))
            }
        }

    }
    
    func resetAnt(tap: UILongPressGestureRecognizer) {
        let point = tap.locationInView(view)
        if let cellz = boardGame.checkPointInCell(point) {
            if cellz.memo == "brick" {
                cellz.view.transform = CGAffineTransformIdentity
            }
        }
        
    }
    func onSwipe(z: UISwipeGestureRecognizer) {
        
    }
    
//    func onTimer() {
//        moveHalfBrick(part1)
//        moveHalfBrick(part2)
//    }
//    
    func moveHalfBrick(antPart: UIView) {
        
    }
    
}
