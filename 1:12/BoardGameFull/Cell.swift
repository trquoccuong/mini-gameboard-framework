//
//  Cell.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 12/31/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class Cell {
    var col: Int
    var row: Int
    var size: CGFloat
    var view: UIView!
    var memo: String!
    var anchorCell : Cell!
    
    init(row: Int, col: Int, size: CGFloat){
        self.col = col
        self.row = row
        self.size = size
        self.view = UIView(frame: CGRectMake(0, 0, size, size))
    }
    
    func draw(view: UIView) {
        view.addSubview(self.view)
    }
    
    func changeLocation(cell:Cell) {
        self.col = cell.col
        self.row = cell.row
    }
    
    func setCenter(board: Board) {
        self.view.center = CGPointMake(board.margin + (0.5 + CGFloat(self.col) * (board.sizeCell + board.marginCell)), 64 + board.margin + (0.5 + CGFloat(self.row) * (board.sizeCell + board.marginCell)))
    }
    // add show // hide
    func hide() {
        self.view.alpha = 0
    }
    
    func show() {
        self.view.alpha = 1
    }
    
    
}

infix operator == {}
//Equal operator
func == (cell1: Cell, cell2: Cell) -> Bool {
    if (cell1.row == cell2.row) && (cell1.col == cell2.col) {
        return true
    } else {
        return false
    }
}






