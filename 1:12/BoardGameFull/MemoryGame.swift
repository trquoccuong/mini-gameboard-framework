//
//  MemoryGame.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import UIKit
import Foundation

class MemoryGame: UIViewController {
    var gameBoard : MemoryBoard!
    var arrayImageView = ArrayObjectGame()
    var arrayImageName :[String: Int] = ["anger" : 2,"bad_egg" : 2,"bad_smile" : 2,"cry" : 2,"exciting" : 2,"girl": 2,"haha" : 2,"happy" : 2,"horror" : 2,"nothing" : 2,"scorn" : 2,"shame" : 2,"shocked" : 2,"victory" : 2]
    
    var arrayFlipView = ArrayObjectGame()
    var count  = 0
    var tempMemory = [Cell]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.whiteColor()
        gameBoard = MemoryBoard(cols: 3, sizeCell: 60, marginCell: 15, paddingCell: 3)
        gameBoard.createGame(view.frame)
        gameBoard.drawGameBoard(view)
        println("\(gameBoard.arrayCell.count)")
        
        // tao layer image
        for cell in gameBoard.arrayCell {
            var memoryCell = MemoryCell(board: gameBoard)
            memoryCell.cell = Cell(row: cell.row, col: cell.col, size: gameBoard.sizeCell)
            var arrayName = filterByValue(arrayImageName)
            
            var randomNumber = Int(arc4random_uniform(UInt32(arrayName.count)))
            memoryCell.cell.view = UIImageView(image: UIImage(named: arrayName[randomNumber]))
            // println("\(arrayName[randomNumber])")
            if let value = arrayImageName[arrayName[randomNumber]] {
                if value != 0 {
                    arrayImageName[arrayName[randomNumber]] = value  - 1
                }
            }
            memoryCell.cell.memo = arrayName[randomNumber]
            
            memoryCell.cell.view.frame.size = CGSize(width: cell.view.frame.width - 5, height: cell.view.frame.width - 5)
            memoryCell.setCenter()
            arrayImageView.ArrayObjectGame.append(memoryCell)
            memoryCell.cell.draw(view)
        }
        
        //tao layer fill
        for cell in gameBoard.arrayCell {
            var memoryCell = MemoryCell(board: gameBoard)
            memoryCell.cell = Cell(row: cell.row, col: cell.col, size: gameBoard.sizeCell)
            memoryCell.cell.view.frame =  cell.view.frame
            memoryCell.cell.view.backgroundColor = UIColor.blackColor()
            memoryCell.setCenter()
            arrayFlipView.ArrayObjectGame.append(memoryCell)
            memoryCell.cell.draw(view)
        }
    }
    
    func filterByValue(dic : [String: Int]) -> [String] {
        var newArray = [String]()
        for (key,value) in dic {
            if value != 0 {
                newArray.append(key)
            }
        }
        return newArray
    }
    

    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        for touch in touches {
            var point = touch.locationInView(view)
            if let cell = gameBoard.checkPointInCell(point) {
                if count < 2 {
                    arrayFlipView.changeAlpha(cell, alpha: 0)
                    count += 1
                    tempMemory.append(cell)
                }
                // check to cell have same memo
                if tempMemory.count == 2 {
                    if let image1 = arrayImageView.getCell(tempMemory[0]) {
                        if let image2 = arrayImageView.getCell(tempMemory[1]) {
                            if image1.cell.memo == image2.cell.memo {
                                tempMemory = []
                                count = 0
                            } else {
                                delay(0.5) {
                                    for cellz in self.tempMemory {
                                        self.arrayFlipView.changeAlpha(cellz, alpha: 1)
                                    }
                                    self.count = 0
                                    self.tempMemory = []
                                }
                            }

                        }
                    }
                }
                
            }
        }
    }
    

}
