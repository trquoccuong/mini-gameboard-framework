//
//  TetrisGame.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class TetrisGame: UIViewController {
    var gameBoard : TetrisBoard!
    var block : TetrisBlock!
    var timer : NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackColor()
        gameBoard = TetrisBoard(cols: 8, sizeCell: 34)
        gameBoard.createGame(view.frame)
        gameBoard.drawGameBoard(view)
        
        block = TetrisBlock(board: gameBoard)
        block.create()
        block.draw(view)


        // Do any additional setup after loading the view.
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "update", userInfo: nil, repeats: true)
    }

    func update() {
        block.moveRightContinuos(1)
        
    }



}
