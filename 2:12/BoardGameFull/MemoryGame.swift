//
//  MemoryGame.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import UIKit
import Foundation

class MemoryGame: UIViewController {
    var gameBoard : MemoryBoard!
    var imageView : ObjectGame!
    var arrayImageName :[String: Int] = ["anger" : 2,"bad_egg" : 2,"bad_smile" : 2,"cry" : 2,"exciting" : 2,"girl": 2,"haha" : 2,"happy" : 2,"horror" : 2,"nothing" : 2,"scorn" : 2,"shame" : 2,"shocked" : 2,"victory" : 2]
    
    var flipView : ObjectGame!
    var count  = 0
    var tempMemory = [Point]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.whiteColor()
        gameBoard = MemoryBoard(cols: 4, sizeCell: 60, marginCell: 2, paddingCell: 0)
        gameBoard.createGame(view.frame)
        gameBoard.drawGameBoard(view)
        
        // tao layer image
        imageView = ObjectGame(board: gameBoard)
        for point in gameBoard.arrayPoint {
            var memoryCell = Cell(row: point.row, col: point.col, size: gameBoard.sizeCell)
            var arrayName = filterByValue(arrayImageName)
            
            var randomNumber = Int(arc4random_uniform(UInt32(arrayName.count)))
            memoryCell.view = UIImageView(image: UIImage(named: arrayName[randomNumber]))
            // println("\(arrayName[randomNumber])")
            if let value = arrayImageName[arrayName[randomNumber]] {
                if value != 0 {
                    arrayImageName[arrayName[randomNumber]] = value  - 1
                }
            }
            memoryCell.memo = arrayName[randomNumber]
            
            memoryCell.view.frame.size = CGSize(width: gameBoard.sizeCell - 5, height: gameBoard.sizeCell - 5)
            memoryCell.setCenter(gameBoard)
            imageView.arrayCell.append(memoryCell)
            memoryCell.draw(view)
        }
        
        //tao layer fill
        flipView = ObjectGame(board: gameBoard)
        for point in gameBoard.arrayPoint {
            var memoryCell = Cell(row: point.row, col: point.col, size: gameBoard.sizeCell)
//            memoryCell.cell.view.frame =  cell.view.frame
            memoryCell.view.backgroundColor = UIColor.blackColor()
            memoryCell.setCenter(gameBoard)
            flipView.arrayCell.append(memoryCell)
            memoryCell.draw(view)
        }
    }


    func filterByValue(dic : [String: Int]) -> [String] {
        var newArray = [String]()
        for (key,value) in dic {
            if value != 0 {
                newArray.append(key)
            }
        }
        return newArray
    }
    
//    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
//        for touch in touches {
//            var point = touch.locationInView(view)
//            
//            if let pointz = gameBoard.checkPoint(point) {
////            println("\(Int((point.x - gameBoard.margin) / (gameBoard.sizeCell + gameBoard.marginCell)))")
//                println("\(pointz.row)")
//                println("\(pointz.col)")
//                println("\(point.x)")
//                println("\(point.y)")
//            }
//        }
//    }


    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        for touch in touches {
            var point = touch.locationInView(view)
            if let pointz = gameBoard.checkPoint(point) {
                if count < 2 {
                    println("\(pointz.row)")
                    println("\(pointz.col)")
                    flipView[pointz.row,pointz.col]?.hide()
                    count += 1
                    tempMemory.append(pointz)
                }
                // check to cell have same memo
                if tempMemory.count == 2 {
                    if let image1 = imageView[tempMemory[0].row,tempMemory[0].col] {
                        if let image2 = imageView[tempMemory[1].row,tempMemory[1].col] {
                            if image1.memo == image2.memo {
                                tempMemory = []
                                count = 0
                            } else {
                                delay(0.5) {
                                    for pointy in self.tempMemory {
                                        self.flipView[pointy.row,pointy.col]?.show()
                                    }
                                    self.count = 0
                                    self.tempMemory = []
                                }
                            }
                            
                        }
                        
                    }
                }

            }
        }
    }
}
