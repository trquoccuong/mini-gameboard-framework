//
//  SnakeBoard.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 12/31/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class SnakeBoard: Board {
    override func drawGameBoard(view: UIView) {
        for point in arrayPoint {
            var cell = Cell(row: point.row, col: point.col, size: sizeCell)
            cell.view  = UIImageView(image: UIImage(named: "grass"))
            cell.setCenter(self)
            cell.draw(view)
        }
    }
}
