//
//  Board.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 12/31/14.
//  Copyright (c) 2014 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class Board {
    var cols: Int
    var rows: Int = 0
    var arrayPoint = [Point]() // gameworld  is array cell -> faster if it only array of Point
    var margin: CGFloat!
    var sizeCell: CGFloat
    var paddingCell: CGFloat = 0
    var marginCell: CGFloat = 0
    var sizeBoard: CGSize!
    
    init(cols: Int, sizeCell: CGFloat){
        self.cols = cols
        self.sizeCell = sizeCell
    }
    
    init(cols: Int, sizeCell: CGFloat, marginCell: CGFloat){
        self.cols = cols
        self.sizeCell = sizeCell
        self.marginCell = marginCell
    }
    
    init(cols: Int, sizeCell: CGFloat, marginCell: CGFloat, paddingCell : CGFloat){
        self.cols = cols
        self.sizeCell = sizeCell
        self.marginCell = marginCell
        self.paddingCell = paddingCell
    }
    
    init(cols: Int,rows : Int,sizeBoard: CGSize) {
        self.cols = cols
        self.rows = rows
        let cellWidth = Int(sizeBoard.width) / cols
        let cellHeight =  Int(sizeBoard.height) / rows
        self.sizeCell = CGFloat(cellWidth > cellHeight ? cellHeight : cellWidth)
    }
    
    
    func createGame(sizeView: CGRect) {
        println("\(sizeView.width)")
        margin = (sizeView.width - CGFloat(cols) * (sizeCell + marginCell)) / 2
        println("\(margin)")
        if rows == 0 {
            rows =  Int((sizeView.height - 64.0 - 2.0 * margin) / (sizeCell +  marginCell))
        }
        
        println("\(rows)")
        for row in 0...rows - 1 {
            for col in 0...cols - 1 {
                var point = Point(row, col)
                arrayPoint.append(point)
            }
        }
        
        
    }
    
    func drawGameBoard(view: UIView) {
        //overide this for change layout board
        for point in arrayPoint {
            var cell = Cell(row: point.row, col: point.col, size: self.sizeCell)
            cell.view.frame = CGRectMake(0, 0, sizeCell - paddingCell , sizeCell - paddingCell)
            cell.view.backgroundColor = UIColor.grayColor()
            cell.setCenter(self)
            cell.draw(view)
        }
    }
    
    subscript(row: Int, col: Int) -> Point?{
        get {
            assert(indexIsValid(row, col), "Index out of range")
            for point in arrayPoint {
                if point.col == col && point.row == row {
                    return point
                }
            }
            return nil
        }
        //            return self.arrayCell[row * self.cols + col]
    }
    
    func indexIsValid(row: Int,_ col: Int) -> Bool {
        return ((row <= self.rows  && row >= 0) || (col >= self.cols && col >= 0))
        
    }
    
    // Check cell have point
    
        func checkPoint(point: CGPoint) -> Point? {
            if point.x > margin && point.x < ( margin + CGFloat(cols) * (sizeCell + marginCell)) {
                if point.y > margin && point.y < ( 64 + margin + CGFloat(rows) * (sizeCell + marginCell)) {
                    var demoCol = Int((point.x - margin) / (sizeCell + 2 * marginCell))
                    var demoRow = Int((point.y - margin - 64) / (sizeCell + 2 * marginCell))
                    
                    var row = Int( CGFloat(demoRow) * (sizeCell + marginCell) + marginCell)
                    var col = Int( CGFloat(demoCol) * (sizeCell + marginCell) + marginCell)
                    return Point(demoRow,demoCol)
                }
            }
            return nil
        }
    
    
    
    
}
