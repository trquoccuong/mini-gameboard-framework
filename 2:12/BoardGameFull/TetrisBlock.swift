//
//  TetrisBlock.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit


class TetrisBlock: ObjectGame {
    var length = 3
    
    func create() {
        for i in 0..<length {
            var s = Cell(row: 4, col: i + 2, size: board.sizeCell)
            s.view.backgroundColor = UIColor.redColor()
            s.setCenter(board)
            arrayCell.append(s)
        }
    }
}
