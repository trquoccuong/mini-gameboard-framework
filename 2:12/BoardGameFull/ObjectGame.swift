
//
//  ObjectGame.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class ObjectGame {
    // game object may be array Cell or only one Cell
    
    var arrayCell = [Cell]()
    var cell: Cell!
    var board: Board!
    var anchorCell: Cell! // ideal: ObjGame rotate around anchorCell, or  this is the begin when creat ObjGame
    var startPoint = Point(0,0) // anchor for rotate and startCell for add to view
    
    
    init(board: Board) {
        self.board = board
    }
    // new ideal: use for loop to create a object is not easy  using array of cell  to make Object game will make creat so easy
    
    /* 
     1 - have array Point
    2 - make array Cell from this array
    */
    
    func creatArrayCell(array: [Point],startPoint : Point){
        for point in array {
            var cell = Cell(row: point.row, col: point.col, size: board.sizeCell)
            cell.view.backgroundColor = UIColor.blueColor()
            cell.setCenter(board)
            arrayCell.append(cell)
            cell.hide()
        }
        self.startPoint = startPoint
    }
    
    // sau Khi khoi tao thi di chuyen  obj den cell den o ban ky
    
    func setBegin(beginPoint: Point) {
        var changeCol =  beginPoint.col - startPoint.col
        var changeRow = beginPoint.row - startPoint.row
        // di chuyen toan bo khoi den diem da chon
        
        for cell in arrayCell {
            cell.col += changeCol
            cell.row += changeRow
            cell.setCenter(board)
        }
    }
    // ideal: khi object ko nam trong game board thi hide nam ngoai game board thi show
    
    
    
    
    func setCenter() {
        cell.setCenter(board)
    }
    
    func draw(parrentView: UIView) {
        for cellBody in arrayCell {
            cellBody.draw(parrentView)
        }
        
    }
    //
    func move() {
        
    }
// ------------------------------MOVING----------------------------
    func moveUp(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.row -= i
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            myCell.row -= i
            myCell.setCenter(board)
        }
    }
    
    func moveDown(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.row += i
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            myCell.row += i
            myCell.setCenter(board)
        }
    }
    
    func moveLeft(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.col -= i
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            myCell.col -= i
            myCell.setCenter(board)
        }
    }
    
    func moveRight(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.col += i
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            myCell.col += i
            myCell.setCenter(board)
        }
    }
    
// ------------------------------MOVING THEN REMOVE FORM VIEW----------------------------
    func moveUpAndRemove(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.row -= i
                cell.setCenter(board)
                removeView()
            }
        }
        if let myCell = cell {
            myCell.row -= i
            myCell.setCenter(board)
            removeView()
        }
    }
    
    func moveDownAndRemove(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.row += i
                cell.setCenter(board)
                removeView()
            }
        }
        if let myCell = cell {
            myCell.row += i
            myCell.setCenter(board)
            removeView()
        }
    }
    
    func moveLeftAndRemove(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.col -= i
                cell.setCenter(board)
                removeView()
            }
        }
        if let myCell = cell {
            myCell.col -= i
            myCell.setCenter(board)
            removeView()
        }
    }
    
    func moveRightAndRemove(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                cell.col += i
                cell.setCenter(board)
                removeView()
            }
        }
        if let myCell = cell {
            myCell.col += i
            myCell.setCenter(board)
            removeView()
        }
    }
    

    
// ------------------------------MOVING CONTINUOS----------------------------
    func moveUpContinuos(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                if cell.row == 0 {
                    cell.row = board.rows
                } else {
                    cell.row -= i
                }
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            if cell.row == 0 {
                cell.row = board.rows
            } else {
                cell.row -= i
            }
            myCell.setCenter(board)
        }
    }
    
    func moveDownContinuos(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                if cell.row == board.rows - 1 {
                    cell.row = 0
                } else {
                    cell.row += i
                }
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            myCell.row += i
            myCell.setCenter(board)
        }
    }
    
    func moveLeftContinuos(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                if cell.col == 0 {
                    cell.col = board.cols
                } else {
                    cell.col -= i
                }
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            if cell.col == 0{
                cell.col = board.cols
            } else {
                cell.col -= i
            }
            myCell.setCenter(board)
        }
    }
    
    func moveRightContinuos(i: Int) {
        if arrayCell.count != 0 {
            for cell in arrayCell {
                if cell.col == board.cols - 1 {
                    cell.col = 0
                } else {
                    cell.col += i
                }
                cell.setCenter(board)
            }
        }
        if let myCell = cell {
            if cell.col == board.cols {
                cell.col = 0
            } else {
                cell.col += i
            }
            myCell.setCenter(board)
        }
    }
    
// ------------------------------MOVING AND STOP----------------------------
    
    func moveAlongTo(point: Point) {
        for i in 0..<countElements(arrayCell) - 1 {
            arrayCell[i].changeLocation(Point(arrayCell[i+1].row, arrayCell[i+1].col))
        }
        arrayCell[arrayCell.count - 1].changeLocation(point)
        for cellBody in arrayCell {
            cellBody.setCenter(board)
        }

    }
    func rotate(cell:Cell , angle: CGFloat) {
        /* to rotate a cell around anchor, set vector ox(1,0), vector oy(0,1)
        vector ox_rotate = (1 * cos (angle) - 0 * sin(angle), 1 * sin(angle) + 0 * cos(angle))
        vector oy_rotate = (0 * cos(angle) - 1 * sin(angle), 0 * sin(angle) +  1 * cos(angle))

        cell rotate = cell.x * ox_rotate + cell.y * oy_rotate */

        
    }
    
    subscript(row: Int, col: Int) -> Cell?{
        get {
            assert(indexIsValid(row, col: col), "Index out of range")
            for cell in arrayCell {
                if cell.col == col && cell.row == row {
                    return cell
                }
            }
            return nil
//            return self.arrayCell[row * board.cols + col]
        }
        //            return self.arrayCell[row * self.cols + col]
    }
    
    func indexIsValid(row: Int, col: Int) -> Bool {
        return ((row <= board.rows  && row >= 0) || (col >= board.cols && col >= 0))
        
    }
    func cellIndexIsValid(cell: Cell) -> Bool {
        return ((cell.row <= board.rows  && cell.row >= 0) || (cell.col >= board.cols && cell.col >= 0))
        
    }
    
    // ideal: cell in object when move out of bound will remove form view
    
    func removeView(){
        for cell in arrayCell {
            if cell.col > board.cols - 1 || cell.col < 0 || cell.row > board.rows - 1 || cell.row < 0 {
                cell.view.removeFromSuperview()
            }
        }
    }
    
//    func setLocation(row: Int, col: Int) {
//        anchorCell = Cell(row: row, col: col, size: s)
//    }
    
    // remove cell  in array
    func removeCell(point:Point) {
        var newArray = arrayCell
        arrayCell = []
        while newArray.count != 0 {
            var  z = newArray.removeLast()
            if !(z.col == point.col && z.row == point.row) {
                arrayCell.append(z)
            }
        }
    }
    
    
}

