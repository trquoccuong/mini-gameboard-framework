//
//  TestObj.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/2/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import Foundation

class TestObject: ObjectGame {
    override func move() {
        moveDownContinuos(1)

        for cell in arrayCell {
            if cellIndexIsValid(cell) {
               cell.show()
            }
            if cell.row > board.rows {
                cell.view.removeFromSuperview()
            }
        }
        
        
    }
}