//
//  SnakeGame.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import UIKit

class SnakeGame: UIViewController {
    var boardGame: SnakeBoard!
    var snake: Snake!
    var brick: ObjectGame!
    
    var timer: NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blackColor()
        boardGame = SnakeBoard(cols: 8, sizeCell: 34)
        boardGame.createGame(view.bounds)
        boardGame.drawGameBoard(view)
        
        snake = Snake(board: boardGame)
        snake.draw(view)
        //
        brick = ObjectGame(board: boardGame)
        for i in 1...6 {
            let randomNumber = Int(arc4random_uniform(UInt32(boardGame.arrayPoint.count)))
            let point = boardGame.arrayPoint[randomNumber]
            let brick_z = Cell(row: point.row, col: point.col, size: boardGame.sizeCell)
            brick_z.memo = "brick"
            brick_z.view = UIImageView(image: UIImage(named: "brick"))
            brick_z.setCenter(boardGame)
            brick.arrayCell.append(brick_z)
            brick.draw(view)
        }
        
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: "update", userInfo: nil, repeats: true)
//        // Do any additional setup after loading the view.
        
        var tap = UITapGestureRecognizer(target: self, action: "addBrick:")
        self.view.addGestureRecognizer(tap)
        
//        let tapOnBrick = UITapGestureRecognizer(target: self, action: "zoom:")
//        tapOnBrick.numberOfTapsRequired = 2
//        view.addGestureRecognizer(tapOnBrick)
//        
//        let longPressOnAnt = UILongPressGestureRecognizer(target: self, action: "resetAnt:")
//        view.addGestureRecognizer(longPressOnAnt)
//        
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: "onSwipe:")
//        swipeRight.direction = UISwipeGestureRecognizerDirection.Down
//        view.addGestureRecognizer(swipeRight)


        
    }
    func update() {
        snake.move(brick)
    }
    
    override func viewWillDisappear(animated: Bool) {
        timer.invalidate()
        timer = nil
    }
    
    func addBrick( tap: UITapGestureRecognizer) {
        let point = tap.locationInView(view)
        if let pointClick = boardGame.checkPoint(point) {
            if let cell = brick[pointClick.row,pointClick.col] {
                cell.view.removeFromSuperview()
                brick.removeCell(pointClick)    
            } else {
                let brick_z = Cell(row: pointClick.row, col: pointClick.col, size: boardGame.sizeCell)
                brick_z.memo = "brick"
                brick_z.view = UIImageView(image: UIImage(named: "brick"))
                brick_z.setCenter(boardGame)
                brick.arrayCell.append(brick_z)
                brick.draw(view)
                
            }
        }
    }
    
    
//    func zoom(tap: UITapGestureRecognizer) {
//        let point = tap.locationInView(view)
//        if let cellz = boardGame.checkPointInCell(point) {
//            if cellz.memo == "brick" {
//                cellz.view.transform = CGAffineTransformConcat(cellz.view.transform, CGAffineTransformMakeScale(2, 2))
//            }
//        }
//
//    }
//    
//    func resetAnt(tap: UILongPressGestureRecognizer) {
//        let point = tap.locationInView(view)
//        if let cellz = boardGame.checkPointInCell(point) {
//            if cellz.memo == "brick" {
//                cellz.view.transform = CGAffineTransformIdentity
//            }
//        }
//        
//    }

    
}
