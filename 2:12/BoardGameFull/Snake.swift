//
//  Snake.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class Snake: ObjectGame {
    var length = 5
    override func draw(parrentView: UIView) {
        for i in 0..<length  {
            var snakeBody = Cell(row: board.arrayPoint[i].row, col: board.arrayPoint[i].col, size: board.sizeCell)
            if ( i != length - 1) {
                snakeBody.view.backgroundColor = UIColor.redColor()
                snakeBody.memo = "body"
            } else {
                snakeBody.view.backgroundColor = UIColor.yellowColor()
                snakeBody.memo = "head"
            }
            snakeBody.setCenter(board)
            arrayCell.append(snakeBody)
            snakeBody.draw(parrentView)
        }
    }
    func move(brick: ObjectGame){
        var possibleMoves = [Point]()
        var snakeHead = arrayCell[length - 1]
//        if snakeHead.row > 0 {
//            let leftCell = Cell(row: snakeHead.row - 1, col: snakeHead.col, size: board.sizeCell)
//            if checkCanMove(leftCell) {
//                possibleMoves.append(leftCell)
//            }
//        }
//        
//        if snakeHead.col > 0 {
//            let topCell = Cell(row: snakeHead.row, col: snakeHead.col - 1, size: board.sizeCell)
//            if checkCanMove(topCell) {
//                possibleMoves.append(topCell)
//            }
//            
//        }
//        
//        if snakeHead.row < board.rows - 1 {
//            let rightCell = Cell(row: snakeHead.row + 1, col: snakeHead.col, size: board.sizeCell)
//            if checkCanMove(rightCell) {
//                possibleMoves.append(rightCell)
//            }
//        }
//        
//        if snakeHead.col < board.cols - 1 {
//            let bottomCell = Cell(row: snakeHead.row, col: snakeHead.col + 1, size: board.sizeCell)
//            if checkCanMove(bottomCell) {
//                possibleMoves.append(bottomCell)
//            }
//        }
        

        
        for i in 1...4 {
            var row = snakeHead.row
            var col = snakeHead.col
            
            switch i {
            case 1: row--
            case 2: col++
            case 3: col--
            case 4: row++
            default:
                return
            }
            
            if !((row > board.rows - 1 || row < 0) || (col > board.cols - 1 || col < 0)) {
                var point = Point(row, col)
                if checkCanMove(point, brick: brick) {
                    possibleMoves.append(point)
                }
            }
            
        }
  //      println("\(countElements(possibleMoves))")

        switch (possibleMoves.count) {
        case 0:
            return //Tit.
        case 1:
            moveAlongTo(possibleMoves[0])
        default:
            let index = arc4random_uniform(UInt32(possibleMoves.count))
            moveAlongTo(possibleMoves[Int(index)])
        }

    }
    
    
    
    func checkCanMove(point: Point,brick: ObjectGame) -> Bool {
        
        if let z =  brick[point.row,point.col] {
        //    println("\(cell.row),\(cell.col), \(cell.row * board.cols + cell.col),\(board[cell.row,cell.col].row),\(board[cell.row,cell.col].col), ")
            return false
        }
        
        // check body snake
        if let k = self[point.row,point.col] {
            return false
        }
        
        return true
    }
}
