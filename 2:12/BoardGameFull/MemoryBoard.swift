//
//  MemoryBoard.swift
//  BoardGameFull
//
//  Created by Tran Quoc Cuong on 1/1/15.
//  Copyright (c) 2015 Tran Quoc Cuong. All rights reserved.
//

import Foundation
import UIKit

class MemoryBoard: Board {
    override init(cols: Int, sizeCell: CGFloat, marginCell: CGFloat, paddingCell: CGFloat) {
        super.init(cols: cols, sizeCell: sizeCell, marginCell: marginCell, paddingCell: paddingCell)
    }
    override func drawGameBoard(view: UIView) {
        for point in arrayPoint {
            var cell = Cell(row: point.row, col: point.col, size: sizeCell)
            cell.view.frame = CGRectMake(0, 0, sizeCell - paddingCell  , sizeCell - paddingCell)
            cell.view.backgroundColor = UIColor.grayColor()
            cell.view.alpha = 0.2
            cell.setCenter(self)
            cell.draw(view)
        }
    }
}
